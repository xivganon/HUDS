local HUDS = {
    name = "Hud Switcher",
    version = "1.2.0",

    nextPulse = 0
}

-- * settings

HUDS.GUI = {
    open = false,
    visible = false
}

HUDS.settings = {
    enabled = false,
    debug = false,
    throttle = 400,
    defaultLayout = 1;
    currentLayout = 0;
    conditionItems = {
        
    }
}

-- * main functions

function HUDS.Init()
    -- Menu entry
    ml_gui.ui_mgr:AddSubMember({id = "HUDS##AA", name = "HUDS", onClick = HUDS.ToggleGui, tooltip = "HUD Switcher"},"FFXIVMINION##MENU_HEADER", "AA##AA")
end

-- Initialize setting values and restore them
function HUDS.InitSettings()

    -- Make sure the profile settings are valid and initialized
    local settings = Settings.HUDS
    settings.enabled = settings.enabled or false
    settings.debug = settings.debug or false
    settings.throttle = settings.throttle or 400
    settings.defaultLayout = settings.defaultLayout or 1
    settings.conditionItems = settings.conditionItems or {}

    -- Now copy the values to a local variable
    HUDS.settings.enabled = settings.enabled
    HUDS.settings.debug = settings.debug
    HUDS.settings.throttle = settings.throttle
    HUDS.settings.defaultLayout = settings.defaultLayout
    HUDS.settings.conditionItems = settings.conditionItems

end

function HUDS.Draw(event, ticks)
    if(HUDS.GUI.open) then
        -- GUI:ShowTestWindow(true)
        -- GUI:ShowMetricsWindow(true)
        
        -- Save settings check
        local save = false

        --Build Hud
        GUI:SetNextWindowPosCenter(GUI.SetCond_FirstUseEver)
        HUDS.GUI.visible, HUDS.GUI.open = GUI:Begin("HUD Switcher##HUDS", HUDS.GUI.open, GUI.WindowFlags_AlwaysAutoResize+GUI.WindowFlags_NoResize)

            -- Enable Checkbox
            HUDS.settings.enabled, save = GUI:Checkbox("Enable##HUDS",HUDS.settings.enabled)

            GUI:Spacing()
            GUI:Spacing()
            GUI:Spacing()
            GUI:Spacing()

            -- Select Default layout
            GUI:PushItemWidth(50)
            HUDS.settings.defaultLayout, save = GUI:Combo("Default Hud Layout##HUDS", HUDS.settings.defaultLayout, {"1","2","3","4"})
            GUI:PopItemWidth()

            GUI:Spacing()
            GUI:Spacing()
            GUI:Spacing()
            GUI:Spacing()

            -- Check if there are any conditions to be listed
            if next(HUDS.settings.conditionItems) ~= nil then

                GUI:Separator() 

                -- Iterate through conditions
                for i,c in pairs(HUDS.settings.conditionItems) do

                    

                    GUI:Separator()
                    GUI:Spacing()

                    -- Draw job select
                    GUI:PushItemWidth(50)
                    c.job, save = GUI:Combo("##HUDSJob"..tostring(i), c.job, Map(HUDS.J, function(job) return job.name end ))
                    
                    -- Draw condition select
                    GUI:SameLine()
                    GUI:PushItemWidth(140)
                    c.condition, save = GUI:Combo("##HUDSCondition"..tostring(i), c.condition, Map(HUDS.C, function(condition) return condition.name end ))

                    -- Draw layout select
                    GUI:SameLine()
                    GUI:Text("HUD Layout: ")
                    GUI:PushItemWidth(30)
                    GUI:SameLine()
                    c.hud, save = GUI:Combo("##HUDSLayout"..tostring(i), c.hud, {"1","2","3","4"})
                    GUI:PopItemWidth()

                    -- Move up on priority
                    GUI:SameLine()
                    if(GUI:ArrowButton("##HUDSMoveUp"..tostring(i), GUI.Dir_Up)) then
                        if(i > 1) then
                            HUDS.ShiftPriority(HUDS.settings.conditionItems, i, i-1)
                            save = true
                        end
                    end

                    -- Move down on priority
                    GUI:SameLine()
                    if(GUI:ArrowButton("##HUDSMoveDown"..tostring(i), GUI.Dir_Down)) then
                        if(i < table.size(HUDS.settings.conditionItems)) then
                            HUDS.ShiftPriority(HUDS.settings.conditionItems, i, i+1)
                            save = true
                        end
                    end

                    -- Add a "delete" button
                    GUI:SameLine()
                    if(GUI:Button("Delete##HUDSDelete"..tostring(i))) then

                        -- Remove the table entry
                        table.remove(HUDS.settings.conditionItems, i)
                        save = true

                    end

                    GUI:Spacing()
                    GUI:Separator() 

                end
                GUI:Separator() 
             end

             GUI:Spacing()
             GUI:Spacing()
             GUI:Spacing()
             GUI:Spacing()
            
            -- Add new condition + hud combo to the list
            if(GUI:Button("Add new HUD setting")) then
                table.insert(HUDS.settings.conditionItems, {condition = 0, job = 0, hud = 0})
                save = true
            end

            -- Save if a change was triggered
            if(save) then
                HUDS.SaveSettings()
            end

            -- DEBUG
            -- GUI:Text(tostring(HUDS.settings.conditionItems))
            
        GUI:End()
    end
end

function HUDS.Update(_, tickcount)
    
    -- Check if the switcher is enabled and it is time to update
    if (HUDS.settings.enabled and HUDS.nextPulse < tickcount) then

        -- Helper to stay set the default hud
        local default = true

        -- Go through the pairs in order (thus, the check the fits first will be the one used, creating a priority)
        for i,c in pairs(HUDS.settings.conditionItems) do

            -- Do the condition test
            if(HUDS.C[c.condition].test() and HUDS.J[c.job].test()) then

                -- Check if the current layout is different that the one matched
                if (HUDS.settings.currentLayout ~= c.hud) then

                    -- Change layout
                    SendTextCommand("/hudlayout "..tostring(c.hud))
                    HUDS.settings.currentLayout = c.hud

                end 
                
                -- Disable default hud
                default = false

                -- Stop checking the pairs (which means that lower placed conditions will be disregarded)
                break

            end
        end

        -- If no check was matched, trigger the default layout
        if(default) then

            -- Check if we are not on the default layout
            if (HUDS.settings.currentLayout ~= HUDS.settings.defaultLayout) then

                -- Switch to the default layout
                SendTextCommand("/hudlayout "..tostring(HUDS.settings.defaultLayout))
                HUDS.settings.currentLayout = HUDS.settings.defaultLayout

            end
        end

        -- Throttle pulse as we don't need to run the logic that often
        HUDS.nextPulse = tickcount + HUDS.settings.throttle

    end
    if (HUDS.settings.enabled == false) then
        HUDS.nextPulse = 0
    end
end

-- ANCHOR auxiliary functions

-- Open/Close GUI
function HUDS.ToggleGui()
    HUDS.GUI.open = not HUDS.GUI.open
end

function HUDS.SetConditionPair(ctype, hnumber)

end

function HUDS.ShiftPriority(t, old, new)
    table.insert(t, new, table.remove(t, old))
end

function HUDS.SaveSettings()
    Settings.HUDS.enabled = HUDS.settings.enabled
    Settings.HUDS.debug = HUDS.settings.debug
    Settings.HUDS.throttle = HUDS.settings.throttle
    Settings.HUDS.defaultLayout = HUDS.settings.defaultLayout
    Settings.HUDS.conditionItems = HUDS.settings.conditionItems
end

function Map(tab, fn)
    newTab = {}
    for k,v in pairs(tab) do
        table.insert(newTab, fn(v))
    end
    return newTab
end

RegisterEventHandler("Module.Initalize",HUDS.Init, "HUDSInit")
RegisterEventHandler("Module.Initalize",HUDS.InitSettings, "HUDSInitSettings")
RegisterEventHandler("Gameloop.Draw", HUDS.Draw, "HUDSDraw")
RegisterEventHandler("Gameloop.Update", HUDS.Update, "HUDSUpdate")

-- * Databases

HUDS.J = {

    -- ! Any job

    {
        name = "ANY",
        test = function () return true end
    },

    -- ! Tanks
    {
        name = "PLD",
        test = function () return (Player.job == FFXIV.JOBS.PALADIN) or (Player.job == FFXIV.JOBS.GLADIATOR)  end
    },
    {
        name = "WAR",
        test = function () return (Player.job == FFXIV.JOBS.WARRIOR) or (Player.job == FFXIV.JOBS.MARAUDER) end
    },
    {
        name = "DRK",
        test = function () return Player.job == FFXIV.JOBS.DARKKNIGHT end
    },
    {
        name = "GNB",
        test = function () return Player.job == FFXIV.JOBS.GUNBREAKER end
    },

    -- ! Healers

    {
        name = "WHM",
        test = function () return (Player.job == FFXIV.JOBS.WHITEMAGE) or (Player.job == FFXIV.JOBS.CONJURER) end
    },
    {
        name = "SCH",
        test = function () return Player.job == FFXIV.JOBS.SCHOLAR end
    },
    {
        name = "AST",
        test = function () return Player.job == FFXIV.JOBS.ASTROLOGIAN end
    },

    -- ! Melee DPS
    {
        name = "MNK",
        test = function () return (Player.job == FFXIV.JOBS.PUGILIST) or (Player.job == FFXIV.JOBS.MONK) end
    },
    {
        name = "DRG",
        test = function () return (Player.job == FFXIV.JOBS.LANCER) or (Player.job == FFXIV.JOBS.DRAGOON) end
    },
    {
        name = "NIN",
        test = function () return (Player.job == FFXIV.JOBS.ROGUE) or (Player.job == FFXIV.JOBS.NINJA) end
    },
    {
        name = "SAM",
        test = function () return Player.job == FFXIV.JOBS.SAMURAI end
    },

    -- ! Phys Ranged DPS
    {
        name = "BRD",
        test = function () return (Player.job == FFXIV.JOBS.ARCER) or (Player.job == FFXIV.JOBS.BARD) end
    },
    {
        name = "MCH",
        test = function () return Player.job == FFXIV.JOBS.MACHINIST end
    },
    {
        name = "DNC",
        test = function () return Player.job == FFXIV.JOBS.DANCER end
    },

    -- ! Mag Ranged DPS
    {
        name = "BLM",
        test = function () return (Player.job == FFXIV.JOBS.THAUMATURGE) or (Player.job == FFXIV.JOBS.BLACKMAGE) end
    },
    {
        name = "SMN",
        test = function () return (Player.job == FFXIV.JOBS.ARCANIST) or (Player.job == FFXIV.JOBS.SUMMONER) end
    },
    {
        name = "RDM",
        test = function () return Player.job == FFXIV.JOBS.REDMAGE end
    },
    {
        name = "BLU",
        test = function () return Player.job == FFXIV.JOBS.BLUEMAGE end
    },
}

-- Condition List with tabs
HUDS.C = {
    {
        name = "Any",
        test = function () return true end
    },
    {
        name = "In City",
        test = function () return IsCityMap(Player.localmapid) end
    },
    {
        name = "In Duty",
        test = function () return InInstance() end
    },
    {
        name = "In Combat",
        test = function () return Player.incombat end
    },
    {
        name = "In World Combat",
        test = function () return Player.incombat and (not InInstance()) end
    },
    {
        name = "In Duty Combat",
        test = function () return Player.incombat and InInstance() end
    }
}